
import pandas as pd
import numpy as np
import datetime as dt
import os

def create_df(symbol_dict):
    '''
    function to compile the data into dataframes
    parameter:
    dict : symbol name and empty dataframe
    returns a dict of a dict
    {orig key: {symbol:, dataframe:}}
    '''
    df_names = pd.read_csv('./support/symbol_name.csv')

    for file in os.listdir('./data'):

        df = pd.read_csv(os.path.join('./data', file), skiprows = 3)

        df['period'] = df['Symbol'].map(lambda x: x.split('/')[1].split('[')[1].split(']')[0])
        df['symbol'] = df['Symbol'].map(lambda x: x.split('/')[1].split('[')[0])

        df['date'] = dt.datetime(*[int(x) for x in file.split('-')[:3]])

        df = pd.merge(df, df_names, how = 'outer', left_on='symbol', right_on='symbol')

        df.drop(labels=['Unnamed: 0', 'Symbol'], axis = 1, inplace = True)

        # at first thought of setting the 0's to the close
        # rethought: because TR is a difference then should leave the 0's
        # outcome: make TR equal to the previous TR

        for key, items in symbol_dict.items():
            items['dataframe'] = items['dataframe'].append(df.ix[df['symbol'] == items['symbol']], ignore_index = True)

    return (symbol_dict)

def apply_rules(df, term = 'short', drop_extra_columns=True):
    '''
    columns needed:
    'high', 'low', 'close'
    '''

    # set the long_day and short_day for breakout and stop
    if term == 'short':
        large_day = 20
        small_day = 10
    elif term == 'long':
        large_day = 55
        small_day = 20
    else:
        print('term: either long or short')

    df['hl_diff'] = abs(df['high'] - df['low'])
    df['hc_diff'] = abs(df['high'] - df['close'].shift())
    df['lc_diff'] = abs(df['low'] - df['close'].shift())

    # set the tr initially to null
    df['tr'] = np.nan
    # calculate tr if high is not 0 (not captured)
    mask_high_zero = df['high'] == 0
    df.ix[~mask_high_zero, 'tr'] = df.ix[~mask_high_zero, ['hl_diff', 'hc_diff', 'lc_diff']].max(axis = 1)

    # run over th data frame as long as there is a null tr
    # use temp to break (want to avoid the possible infinite loop
    temp = 0
    while df['tr'].isnull().any():
        # save the shift of tr using the full dataframe
        # otherwise the mask won't get to the right shift
        df['shift_tr'] = df['tr'].shift()
        mask_tr_null = df['tr'].isnull()
        df.ix[mask_tr_null, 'tr'] = df.ix[mask_tr_null, 'shift_tr']
        if temp >= 4:
            break
        temp += 1
    # drop the shift tr
    df.drop(['shift_tr'], axis=1, inplace = True)

    # set high, low, open from 0 to close
    df.ix[df['high'] == 0, 'high'] = df.ix[df['high'] == 0, 'close']
    df.ix[df['low'] == 0, 'low'] = df.ix[df['low'] == 0, 'close']
    df.ix[df['open'] == 0, 'open'] = df.ix[df['open'] == 0, 'close']
    
    # N = (19 * PDN + TR) / 20
    df['n_moving_average'] = pd.rolling_mean(df['tr'], 20)

    # first is the average tr of the first 20 rows
    df.ix[19, 'n_pdf_calc'] = df.ix[:19, 'tr'].mean()
    for i in range(20, df.shape[0]):
        df.ix[i:, 'n_pdf_calc'] = (19 * df.ix[i - 1, 'n_pdf_calc'] + df.ix[i, 'tr']) / 20

    # want to shift the index by one so that it saves one day later
    # in other words the day you are looking at is the high for previous 20 days and excludes the present day
    df['large_day_high'] = pd.rolling_max(df['high'], large_day).shift()
    df['small_day_low'] = pd.rolling_min(df['low'], small_day).shift()

    # Breakout - 20 day high or low
    df['breakout'] = df['high'] > df['large_day_high']

    # Adding units
    # 0.5 * N (based on first breakout)
    # compare to the 20 day high because that is when the units will be bought
    df['adding_units'] = df['large_day_high'] + (df['n_pdf_calc'] * 0.5)

    # Stop
    # 2 * N
    # remains the same for all units (if 27 is stop on first unit, 27 is stop for all following units)
    # compare to the 20 day high because that is when the units will be bought
    df['stop'] = df['large_day_high'] - (df['n_pdf_calc'] * 0.5)

    # Exit - 10 day low
    df['exit'] = df['low'] < df['small_day_low']

    if drop_extra_columns:
        df.drop(['symbol', 'hl_diff', 'hc_diff', 'lc_diff', 'n_moving_average'], axis = 1, inplace = True)

    return(df)

def create_df_units(df_data):
    # runs through dataframe.
    # checks for breakouts, adds units
    # sells at stops and exits

    def add_buy(index, row):
        # if breakout
        # save index of the row that triggered
        # save n
        # save bought = 20_day_high
        # save stop
        row_dict = {'bought_index': [index], 'bought': [row['large_day_high']], \
                                            'n': [row['n_pdf_calc']], 'stop': [(row['large_day_high'] - \
                                                                                     2 * row['n_pdf_calc'])]}
        return(df_units.append(pd.DataFrame(row_dict)))

    df_units = pd.DataFrame(columns = ['bought_index', 'sold_index', 'bought', 'sold', 'n', 'stop'])

    for index, row in df_data.iterrows():
        # if df_units is empty then looking for a buy
        if df_units.empty:
            if row['high'] >= row['large_day_high']:
                df_units = add_buy(index, row)
        else:
            # breakout
            # if any sold in df_units is null
            if df_units['sold'].isnull().any():
                # check for exit
                if row['low'] <= row['small_day_low']:
                    df_units.ix[df_units['sold'].isnull(), ['sold', 'sold_index']] = [row['small_day_low'], index]
                # check for stop
                elif row['low'] <= row['stop']:
                    df_units.ix[df_units['sold'].isnull(), ['sold', 'sold_index']] = [row['stop'], index]
                # check for add unit
                # 1/2 N above last bought unit
                elif row['high'] >= df_units.iloc[-1]['n'] * 0.5 + df_units.iloc[-1]['bought']:
                    df_units = add_buy(index, row)
            else:
                # look for buy
                if row['high'] >= row['large_day_high']:
                    df_units = add_buy(index, row)

    df_units.drop(labels=['stop'], axis=1, inplace=True)
    df_units.reset_index(inplace = True, drop=True)
    return df_units